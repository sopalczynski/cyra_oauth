from django.apps import AppConfig


class CyraOauthExampleConfig(AppConfig):
    name = 'cyra_oauth_example'
