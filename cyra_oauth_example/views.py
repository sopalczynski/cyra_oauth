from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from oauth2_provider.views.generic import ProtectedResourceView

# Create your views here.
from cyra_oauth_example.forms import RegisterForm


def register_view(request):
    form = RegisterForm()

    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            # create a user here
            user_model = get_user_model()
            user = user_model()
            user.username = form.cleaned_data.get('username')
            user.set_password(form.cleaned_data.get('retype_password'))
            user.save()
            return HttpResponse("You have registered, user_id: {}".format(user.id))

    return render(request, 'registration/register.html', {'form': form})


@login_required
def profile_view(request):
    return HttpResponse("User id: {}".format(request.user.id))


class ApiEndpoint(ProtectedResourceView):

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'user_id': request.user.id,
            'username': request.user.username,
        })
