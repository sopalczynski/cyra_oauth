from django import forms


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=20, required=True)
    password = forms.CharField(widget=forms.PasswordInput, max_length=10, required=True)
    retype_password = forms.CharField(widget=forms.PasswordInput, max_length=10, required=True)

    def clean_retype_password(self):
        password = self.cleaned_data.get('password')
        retype_password = self.cleaned_data.get('retype_password')

        if password != retype_password:
            raise forms.ValidationError("Password do not match.")

        return retype_password
