Steps.

1. Run this app: `python manage.py runserver 0:8080`

The database is already here - no need for migration.
But probably you will need to install the requirements: pip install -r requirements.txt

2. Go to: `http://127.0.0.1:8080/register/` in yout browser.
3. Create an account and store username and remember it.
4. Go to: `http://127.0.0.1:8080/login/` in your browser. Log in using the provided credentials.
5. Go to: `http://127.0.0.1:8080/o/applications/` and create an oauth application, with data provided below:

* Authorization grant type: Resource owner password-based
* Client type: public
* Name: pick some describing one

6. Now we will use an API.
7. Obtain token under: `http://127.0.0.1:8080/o/token/`

Use POST method and x-www-form-urlencoded as content-type, with data below:

* grant_type=password
* client_id=client_id_of_your_app
* client_secret=client_secret_of_your_app
* username=a_username_of_the_user_you_want_to_login
* password=a_password_for_the_user_mentioned_above

You should get the following response:

```
{
  "access_token": "vpGR6XtKlz5wHWUBHKoghm6ni0fQjE",
  "token_type": "Bearer",
  "expires_in": 36000,
  "refresh_token": "LmFI2ybxUZ5ZJOq9E81QvJx1ifSX7d",
  "scope": "read write"
}
```

8. Use: `http://127.0.0.1:8080/api/view/`

* Method - GET
* Headers - Authorization: Bearer token_obtained_from_the_step_above

You should fet following response:
```
{
  "username": "username_of_your_user",
  "user_id": id_of_the_user
}
```

